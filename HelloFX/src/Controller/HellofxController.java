package Controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author calim
 */
public class HellofxController implements Initializable {

    @FXML
    private Label lblOk;
    @FXML
    private Button btnOk;
    @FXML
    private Button btnOk1;
    @FXML
    private Label lblOk1;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void handlebtnOk(ActionEvent event) {
        lblOk.setText("El botón hizo click");
    }
    @FXML
    private void handlebtnOk1(ActionEvent event) {
        lblOk1.setText("El botón 2 hizo click");
    }
}
